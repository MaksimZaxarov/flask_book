from flask import Flask, render_template, request, escape

from vsearch import search4letters

app = Flask(__name__)


# http://127.0.0.1:5000/search

# @app.route('/')
# def hello() -> '302':
#   return redirect('/entery')

def log_request(req, res: str) -> None:
    with open('vsearch.log', 'a') as log:
        print(req.form, req.remote_addr, req.user_agent,res, file=log, sep='|')
        # print(str(dir(req)), res, file=log)
        # print(req.form, file=log)
        # print(req.remote_addr, file=log)
        # print(req.user_agent, file=log)
        # print(res, file=log)


@app.route('/search', methods=['POST'])
def do_search() -> str:
    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your reasult:'
    results = str(search4letters(phrase, letters))
    log_request(request, results)
    return render_template('results.html',
                           the_phrase=phrase,
                           the_letters=letters,
                           the_title=title,
                           the_results=results, )
    # return str(search4letters(phrase,letters))
    # return str(search4letters('life, universe and everthing', 'eiru,!'))


@app.route('/')
@app.route('/enter')
def entry_page():
    return render_template('entry.html',
                           the_title='Welcome to search41etters on the web!')


# показывает сохраненные данные
@app.route('/viewlog')
def view_the_log() -> str:
    contents = []
    with open('vsearch.log') as log:
        for line in log:
            contents.append([])
            for item in line.split('|'):
                contents[-1].append(escape(item))
    titles = ('From Date', 'Remote_addr', 'User_argent','Results')
    return render_template('viewlog.html',
                           the_title='View Log',
                           the_row_titles=titles,
                           the_data=contents,)
        #contents = log.read()
    return str(contents)


# app.run(debug=True)
if __name__ == '__main__':
    app.run(debug=True)
